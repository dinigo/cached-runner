# Cached Runner
[![pipeline status](https://gitlab.com/dinigo/cached-runner/badges/master/pipeline.svg)](https://gitlab.com/dinigo/cached-runner/commits/master)
[![coverage report](https://gitlab.com/dinigo/cached-runner/badges/master/coverage.svg)](https://gitlab.com/dinigo/cached-runner/commits/master)
[![npm version](https://badge.fury.io/js/cached-runner.svg)](https://www.npmjs.com/package/cached-runner)
[![npmjs downloads](https://img.shields.io/npm/dt/cached-runner.svg)](https://www.npmjs.com/package/cached-runner)

Receives a period of time and a request function, and performs all async
operations to get the data. If you retry the request in a time shorter
than the period it returns the cached result instead. You can also set a
postprocess function and another function that runs every time.

Inmediate after invocation `request -> process -> cached -> allways -> returned`.
New timestamp and new random number:
```
> "1513186989013 --> ~(4)~"
```
One second after invocation data isn't refreshed since period hasn't
expired so: `cached -> allways -> returned`. New timestamp and cached
random number:
```
> "1513186990016 --> ~(4)~"
```
Three secons after first invocation cache has expired, so request is
rerun: `request -> process -> cached -> allways -> returned`. New
timestamp and new random number:
```
> "1513186992017 --> ~(90)~"
```

### Running tests
Using your favorite node package manager, first install dependencies:
```shell
$ yarn
```
And then run the `test` command:
```shell
$ yarn test
```

## Compile to javascript es5:
Once dependencies are installed you just have to run the `build` command
and compiled files will be placed under `dist/` directory:
```shell
$ yarn run build
```
## Lint the code:
If you are making a commit please check beforehand that your code
complies with the linter:
```shell
$ yarn run lint
```

## Examples
### Simple example
Usage
```javascript
const simpleCahedRun = new CachedRun(2500, ()=>Math.random());
simpleCahedRun.start().then(res => console.log(res)); // run instantly
setTimeout(() => simpleCahedRun.start().then(res => console.log(res)),1000); // run after 1 second
setTimeout(() => simpleCahedRun.start().then(res => console.log(res)),3000); // run after 3 secons
setTimeout(() => simpleCahedRun.refresh(),3500); // force refresh
setTimeout(() => simpleCahedRun.start().then(res => console.log(res)),4000); // run after 4 secons```
```
Output
```
> 0.06756653717504979 // run instantly: new cache until 2.5s
> 0.06756653717504979 // run after 1 second: get from cache
> 0.04633045049386242 // run after 3 seconds: new cache until 5s
                      // force refresh
> 0.20170561495875194 // run after 4 seconds: forced refresh
```

### Complete Example
Here we set all available options.
```javascript
const period = 2000;  // ms
const allways = data => +new Date() + ' --> ' + data;  // an allways run function that adds a timestamp
const request = () => Math.random();  // a request function that generates a random number
const process = num => `~(${Math.ceil(num*100)})~`;
const cachedRun = new CachedRun(period, request, process, allways);
cachedRun.start().then(res => console.log(res));
setTimeout(() => cachedRun.start().then(res => console.log(res)),1000);
setTimeout(() => cachedRun.start().then(res => console.log(res)),3000);
```

## Documentation
For further reference you can [take a look here](https://dinigo.gitlab.io/cached-runner/classes/_index_.cachedrun.html)